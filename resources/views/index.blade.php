<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    </head>
    <body>
        <div id="app">
            <company-select-component :message="message" v-on:company-selected="getSupervisors"></company-select-component>
            [[ message ]]
        </div>
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>