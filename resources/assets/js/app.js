require('./bootstrap');
window.Vue = require('vue');

Vue.component('company-select-component', require('./components/CompanySelectComponent'));

const app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#app',
    data: {
        companyID: 0,
        message: 'Hello Vue!'
    },
    methods: {
        getSupervisors: function (value) {
            axios.get('http://tacitapp.com/cp/cp/public/' + value + '/supervisors')
            .then(function (response) {
              console.log(response.data);
              //now you have supervisors data
              //change companyID and
              //and start previwing them
            })
            .catch(function (error) {
              console.log(error);
            });
        }
    },
    created(){
        //Do something at the beginning
    }
});