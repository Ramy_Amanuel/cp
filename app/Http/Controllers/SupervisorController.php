<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SupervisorController extends Controller
{
    /**
     * Display a listing of the supervisors.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($company)
    {
        switch($company){
            case 1:
                $sql = "SELECT uid as 'id', FullName as 'name' FROM nichepha_tabukdb.User where JobTitle = 'SuperVisor'";
                break;
            
            case 2:
                $sql = "SELECT uid as 'id', fullname as 'name' FROM nichepha_chiesi.user where Job = 3";
                break;

            case 3:
                $sql = "SELECT user.id, fullname as 'name' FROM nichepha_dermazone.`user` join nichepha_dermazone.role_user on user.id=role_user.user_id WHERE role_user.role_id in (6,7,9)";
                break;

            default:
                return "Error";

        }
        $supervisors = DB::select($sql);
        return $supervisors;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
