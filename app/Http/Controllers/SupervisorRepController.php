<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SupervisorRepController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($company, $id)
    {
        switch($company){
            case 1:
                $sql = "SELECT User.uid as 'id',User.FullName as 'name' FROM nichepha_tabukdb.`SuperReps`
                        JOIN nichepha_tabukdb.User on SuperReps.rid=User.uid
                        Where supid={$id}";
                break;
            
            case 2:
                $sql = "SELECT user.uid as 'id',user.fullname as 'name' FROM nichepha_chiesi.`relations` 
                JOIN nichepha_chiesi.user on relations.down = user.uid
                Where up={$id}";
                break;

            case 3:
                $sql = "SELECT user.id,user.fullname as 'name' FROM nichepha_dermazone.`user_supervisor`
                JOIN nichepha_dermazone.user on user_supervisor.user_id = user.id
                Where super_id = {$id}";
                break;

            default:
                return "Error";

        }
        $supervisors = DB::select($sql);
        return $supervisors;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
